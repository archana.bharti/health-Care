﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Repository.Models
{
   public class HospitalDeptEntity
    {
        public long Id { get; set; }
        public long HospitalId { get; set; }
        public long DepartmentId { get; set; }      
    }
}
